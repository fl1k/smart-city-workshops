import RPi.GPIO as GPIO
import time
import board
import busio
import adafruit_ads1x15.ads1115 as ADS      # set to adafruit_ads1x15.ads1105 if using an ADS1105 instead of an ADS1115
from adafruit_ads1x15.analog_in import AnalogIn

##
# blink led 'l' n times, each blink during s seconds
def blinkLed (l,n,s):
    for _ in range(n):
        for i in range(100): 
            l.ChangeDutyCycle(i)
            time.sleep(s/200)
        for i in range(100,-1,-1):
            l.ChangeDutyCycle(i)
            time.sleep(s/200)


LED_PIN = 13
SERVO_PIN = 21
SERVO_POSITION_MAX_CLOCKWISE = 0.1
SERVO_POSITION_MAX_COUNTER_CLOCKWISE = 14
SERVO_POSITION_MIDDLE = (SERVO_POSITION_MAX_CLOCKWISE + SERVO_POSITION_MAX_COUNTER_CLOCKWISE) / 2

ADS1115_MINIMUM_VOLTAGE = 0
ADS1115_MAXIMUM_VOLTAGE = 4.096


GPIO.setmode(GPIO.BCM)
GPIO.setup(LED_PIN, GPIO.OUT)
LED = GPIO.PWM(LED_PIN, 100)
LED.start(0)
GPIO.setup(SERVO_PIN, GPIO.OUT)
SERVO = GPIO.PWM(SERVO_PIN, 50)


# LDR Setup
I2C = busio.I2C(board.SCL, board.SDA)   # communication bus
ADS11x5 = ADS.ADS1115(I2C)              # set to ADS.ADS1105(I2C) if using an ADS1105 instead of an ADS1115
CHANNEL = AnalogIn(ADS11x5, ADS.P0)     # setup channel

# blink the led thrice
blinkLed(LED,3,.5)

SERVO.start(SERVO_POSITION_MAX_COUNTER_CLOCKWISE) #left
time.sleep(1)

print(ADS1115_MAXIMUM_VOLTAGE)
print(CHANNEL.voltage)
# let LDR decide the position of the servo until CTRL+C is pressed
while True:
    servo_position = abs(((CHANNEL.voltage / ADS1115_MAXIMUM_VOLTAGE) * SERVO_POSITION_MAX_COUNTER_CLOCKWISE) - SERVO_POSITION_MAX_COUNTER_CLOCKWISE) + 1
    print("{:>5.3f}".format(servo_position))
    SERVO.ChangeDutyCycle(servo_position)
    time.sleep(0.3)

GPIO.cleanup()