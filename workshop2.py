import RPi.GPIO as GPIO
import time

def getDistance ():
    GPIO.output(TRIG, True)
    time.sleep(0.00001)
    GPIO.output(TRIG, False)
    while GPIO.input(ECHO) == False:
        start = time.time()
        
    while GPIO.input(ECHO) == True:
        end = time.time()
        
    sig_time = end - start

    return sig_time / 0.000058
    
def changeLight (light):
    if light != GREEN: # hide green
        GPIO.output(GREEN, GPIO.LOW)
    if light != YELLOW: # hide yellow
        GPIO.output(YELLOW, GPIO.LOW)
    if light != RED: # hide red
        GPIO.output(RED, GPIO.LOW)
    GPIO.output(light, GPIO.HIGH) # show light
    
    

GPIO.setmode(GPIO.BCM)

GREEN = 16
YELLOW = 20
RED = 21
TRIG = 4
ECHO = 18
GPIO.setup(GREEN,GPIO.OUT)
GPIO.setup(YELLOW,GPIO.OUT)
GPIO.setup(RED,GPIO.OUT)
GPIO.setup(TRIG,GPIO.OUT)
GPIO.setup(ECHO,GPIO.IN)


# GPIO.output(RED, GPIO.HIGH) # show light
# time.sleep(10)

while True:
    d = getDistance ()
    print('Distance: {} centimeters'.format(d))
    if d > 50:
        changeLight (GREEN)
    elif d > 20:
        changeLight (YELLOW)
    else:
        changeLight (RED)
    time.sleep(1)

GPIO.cleanup()