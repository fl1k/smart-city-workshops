import RPi.GPIO as GPIO
import time
import signal

# set every led (L \in ls) to LOW
#       used as cleanAllLeds (ARRAY_OF_LED_PINS)
def ledsLow (ls):
    for L in ls:
        GPIO.output(L, GPIO.LOW)
        
# blink leds in order
# assuming ls is ordered from Left to Right
#       used as blinkLeds (ARRAY_OF_LED_PINS, REPETITIONS, SLEEPING_TIME)
def blinkLeds (ls, n, m):
    for _ in range(n):
        for L in ls:
            GPIO.output(L, GPIO.HIGH)       # turn led on
            time.sleep(m)                   # wait for m seconds
            GPIO.output(L, GPIO.LOW)        # turn led off
        
# print the given integer (n) in binary,
# assuming ls is ordered from Left to Right
#       used as showBinary(SOME_INTEGER, ARRAY_OF_LEDS)
def showBinary (n, ls):
    ledsLow (ls)
    i = 0                       # starting index
    lim = 2**(len(ls)-1)        # 2^n
    if not 0 <= n < 2*lim:      # if not expected to be printable
        return False
    else:
        while n > 0:
            if n >= lim:
                GPIO.output(ls[i], GPIO.HIGH)
                n-=lim
            i+=1
            lim/=2
        return True




# Leds' pins ordered from Left to Right (order needed for binary display)
LED_PINS = [21, 20, 16, 12, 25, 24]
PIR_PIN = 6

# setup and turn off all defined LED_PINS
GPIO.setmode(GPIO.BCM)
for L in LED_PINS:
    GPIO.setup(L, GPIO.OUT)
    GPIO.output(L, GPIO.LOW)

# blink leds
blinkLeds (LED_PINS,5,0.05)

time.sleep(1)

# show some binary number
showBinary(21,LED_PINS)

time.sleep(1)

# show some other binary number
showBinary(42,LED_PINS)

time.sleep(1)

# turn off all leds again
ledsLow (LED_PINS)


counter=0

GPIO.setup(PIR_PIN, GPIO.IN)

print ("Waiting for PIR to settle...")
# Loop until PIR output is 0
while GPIO.input(PIR_PIN)==1:
    Current_State = 0    

print ("Ready...")
while True: # Until CTRL+C is pressed
    if GPIO.input(PIR_PIN):
        counter += 1
        showBinary (counter, LED_PINS)
        time.sleep(1)
    time.sleep(0.2)

signal.pause()

GPIO.cleanup()